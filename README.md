thomasfoster.co
===============

This is the plain, basic website I keep running at [thomasfoster.co](http://thomasfoster.co). Check it out if you wish.

I'm keeping the source on Github but I most likely will do everything myself. If you do notice a bug or a spellin mistak, do feel free to send a pull request. I'll thank you with a virtual cup of tea and a lamington.
